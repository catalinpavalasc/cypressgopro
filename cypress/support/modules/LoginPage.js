/// <reference types="Cypress" />

export class LoginPage {

    login(username, password) {
        
        cy.visit('/login')  
        
        cy.get('input[name="loginEmail"]').type(username)

        cy.get('input[name="loginPassword"]').type(password)

        cy.get('button[id="gptest-login-btn"]').click()

        cy.wait(5000)

    }

}