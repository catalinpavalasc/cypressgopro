/// <reference types="Cypress" />

export class ShopPage {

    addCameras(cameras) {
        
        cy.visit('/en/dk/shop/cameras');

        cy.wait(10000);

        cy.get('i[class="icon icon-exit-stroke"]').click({multiple:true,force:true});
    
        cy.wait(5000);

        var i,j;

        for (i = 0; i < cameras.length; i++) {

            j = 2 + i * 2;

            cy.get('input[value=' + cameras[i] + ']').parent().children('button[type="submit"]').click({force:true});

            cy.get(`div[class="mini-cart-header"]`)
                .should('be.visible')
                .should('exist')
                .should('contain', "2 Items added to cart"); 

            cy.get(`div[class="mini-cart-total"]`)
                .should('be.visible')
                .should('exist')
                .should('contain', j);


            cy.get('span[class="icon-exit-stroke"]').click({force:true});

            cy.wait(5000);
            
        }        

    }

}