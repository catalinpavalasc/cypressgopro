/// <reference types="Cypress" />

export class CartPage {

    removeCameras(cameras) {
        // Visiting the shop cart
        cy.visit('/en/dk/shop/cart');               
       
        cy.wait(5000);     
        
        for (var i = 0; i < cameras.length; i++) {
            // This is actually where we delete the items
            cy.get('button[data-id=' + cameras[i] + ']').click( {force:true} );

            cy.wait(5000); 
            // Assume that the button dissappears
            cy.get('button[data-id=' + cameras[i] + ']').should('not.exist');
            
        }
        // Expect cart empty
        cy.get(`div[class="cart-empty"]`)
                .should('be.visible')
                .should('exist')
                .should('contain', "Your cart is empty");

    }

}