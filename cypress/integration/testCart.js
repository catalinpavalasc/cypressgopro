
// This is for the IDE telling that the source code is Cypress-like
/// <reference types="Cypress" />

import { LoginPage } from "../support/modules/LoginPage";
import { ShopPage } from "../support/modules/ShopPage";
import { CartPage } from "../support/modules/CartPage";
import { LogoutPage } from "../support/modules/LogoutPage";

describe('Test cart',function () {

    Cypress.on('uncaught:exception', (err, runnable) => {
        // returning false here prevents Cypress from
        // failing the test, since the AUT returns error events
        return false
      })

      const loginPage = new LoginPage();
      const shopPage = new ShopPage();
      const cartPage = new CartPage();
      const logoutPage = new LogoutPage();

    before("make sure the user is logged in", () => {

        loginPage.login("catalin.pavalasc@gmail.com", "Cristi.07");    

    });

    it('adds to cart two cameras and then deletes them', function () {     

        var cameras = ["CHDHX-701-master","CHDHC-601-master"];

        shopPage.addCameras( cameras );      

        var camerasID = ["CHDHX-701-RW", "CHDHC-601-RW"];

        cartPage.removeCameras( camerasID );       
       
    })

    after("make sure the user is logged out", () => {

        logoutPage.logout();

    });

})