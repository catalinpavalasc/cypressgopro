# README #

Some sample automation tests using Cypress.

### What is this repository for? ###

This repository is a sample framework for some tests on the GoPro e-commerce platform.
For the execution of tests, NPX is needed (if not previously installed), but usually is installed with Cypress.
The tests were coded using Cypress 1.4.2 on a Windows platform (Visual Studio Code 1.41.1)  
but should work on any platform. Please use for execution only Cypress 1.4.2.
Additional documentation about Cypress can be found at:
https://docs.cypress.io/guides/getting-started/installing-cypress.html#System-requirements.


### How do I get set up? ###

Main testing class (or spec in Cypress language) can be found in cypress\integration\testCart.js
The page objects (or support modules in Cypress language) can be found in cypress\support\modules\.
Where the homework requirements specified "assert that the result is correct" a Cypress "should" command
was implemented in order for the test to fail if that element would not met the criteria.

1. Dependecies
2. How to run tests
3. Reports

### Dependencies ###
 
Clone the current repository. 

git clone https://catalinpavalasc@bitbucket.org/catalinpavalasc/cypressgopro.git

In order for the tests to work we will need install:

Cypress

Mocha

Mocha-Junit-Reporter

Use the following command to install cypress (use SUDO for restricted permissions), executed from the root of the repository:

npm install cypress@1.4.2 --save-dev

Use the following command to install mocha:

npm install mocha@7.0.0

Use the following command to install mocha-junit-reporter:

npm install mocha-junit-reporter@1.23.3 --save-dev

### How to run the tests ###

The tests can be run via the Cypress simulator. In order to open the simulator use the following command, executed from the root of the repository:

./node_modules/.bin/cypress open

From the simulator, one must select the testCart.js spec and wait for the execution to start.

Do not update the Cypress version from the simulator's interface.

One other way to launch the spec is via the following command (remember sudo), runned from the root of the repository:

npx cypress run --spec 'cypress/integration/testCart.js' --browser chrome

We can leave aside the --browser chrome parameter if we want to execute headless via the electron browser.

### Reports ###

The Junit results are saved in the reports\junit folder.

